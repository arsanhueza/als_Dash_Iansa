export type AmplifyDependentResourcesAttributes = {
    "api": {
        "demoiansa": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}